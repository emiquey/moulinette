from abc import ABC, abstractmethod
from exercises import *
from dataTypes import *
from constraints import *
from moulinette import *
from misc import *

import argparse
import os
import shutil

parser = argparse.ArgumentParser()
parser.add_argument("--student", help="name of the Python file to be corrected", default="squelette")
parser.add_argument("--corr", help="Python file containing the correction", default="correction")
parser.add_argument("--feedback", help="Text file to store the feedback", default="result.txt")
args = parser.parse_args()


 
# If feedback alreay exists, move it
if os.path.isfile('./'+args.feedback):
    shutil.move('./'+args.feedback,'./old_'+args.feedback)

if args.student[-3:]==".py":
    args.student=args.student[:-3]
if args.corr[-3:]==".py":
    args.corr=args.corr[:-3]



time_limit=10
strictint = Int(1,100)
myint = Int(0,100)
bigint = Int(100,999)
hugeint = Int(0,10**6)
mybool = Bool()
myNone = Empty()
myfloat = Float()
mylist = List(vmin=0,vmax=100,T=Int)
nonemptylist = List(0,100,lmin=1,T=Int)
listwithdistincts = List(0,100,nmin=2,T=Int)
mystr = String('a','e')
shortstr = String('a','e',0,4)
alice = Alice()
zero = Constant(0)
cent = Constant(100)
trace = GPXTrace()
floatlist = List(vmin=1,vmax=2,T=Float)




def exo(name, input, output, nb_test=20,erreur=2,
        time_limit=10,coeff=1, constraints=None):
    return Exercise(name, input, output, test_file=args.student,
                 nb_test=nb_test,erreur=erreur,time_limit=time_limit,coeff=coeff,
                 constraints=constraints,correction=args.corr)

def exo_approx(name, input, output, limit, limit_string,
                 nb_test=20,erreur=2,time_limit=10,coeff=1,
                 constraints=None):
    return ExoApprox(name, input, output,limit, limit_string,  test_file=args.student,
                 nb_test=nb_test,erreur=erreur,time_limit=time_limit,coeff=coeff,
                 constraints=constraints,correction=args.corr)

def exo_with_input(name, input, output, howtocheck, 
                 nb_test=20,erreur=2,time_limit=10,coeff=1,
                 constraints=None):
    return ExoCheckWithInput(name, input, output, howtocheck,test_file=args.student,
                 nb_test=nb_test,erreur=erreur,time_limit=time_limit,coeff=coeff,
                 constraints=constraints,correction=args.corr)



#############################################
def baseIntRec(x):
    return x==0 or x==1

def preRecInt(x):
    return IsRecursive(x,baseIntRec)

def baseRecList(x):
    return len(x)<=1

def preRecList(x):
    return IsRecursive(x,baseRecList)


############# LISTE DES EXERCICES ###########
examen = []

# # Exo 1
# print("args",args.student,args.corr)
# examen.append(exo("somme_liste",mylist,myint))
# examen.append(exo("somme_carres",mylist,myint))
# examen.append(exo("ecart_type",nonemptylist,myfloat))

# # Exo 2
# examen.append(exo("terme",myint,myfloat,constraints=preRecInt,coeff=2))

# def DontPi(x):
#     return DontUse(x,["pi"])
# examen.append(exo_approx("approx",myfloat,myfloat,pi,"π",coeff=2,constraints=DontPi))

# # Exo 3
# # Rajouter la contrainte récursive
# examen.append(exo("max_liste",nonemptylist,myint,constraints=preRecList))
# examen.append(exo("second_max",listwithdistincts,myint,coeff=2))

# #Exo 4
# examen.append(exo("dates",trace,floatlist))
# examen.append(exo("distances",trace,floatlist,coeff=2))
# examen.append(exo("vitesse",trace,floatlist))

# #Exo 5
# examen.append(exo("repete",mystr,mystr))
# examen.append(exo("max_bloc",mystr,myint,coeff=2))

# Exo 6
def baseFacteur(m,M):
    d = len(M)-len(m)
    return d<=0 or M[0:d]==m

def preRecFacteur(x):
    return IsRecursive(x,baseFacteur)

def inInterdit(x):
    if " in " in x and (not ("for" in x[:x.index(" in ")])):
        return False
    return True

def preInInterdit(e):
    return DontUseTest(e,inInterdit,"utilisation du test 'in' interdite !")
        
def preRecInInterdit(e):
    return RecDontUseTest(e,baseFacteur,inInterdit,"utilisation du test 'in' interdite !")

examen.append(exo("facteur",Tuple([String('a','c',1,4),mystr]),mybool,constraints=preRecInInterdit))
examen.append(exo("sous_chaine",Tuple([shortstr,mystr]),mybool,coeff=2,constraints=preInInterdit))


# # Exo 7

# def baseRecBob(alice,a,b,check):
#     return a==b or check((a+b)//2,[alice,a,b]) or check(1+(a+b)//2,[alice,a,b])

# def preRecBob(check):
#     return lambda x:IsRecursive(x,lambda alice,a,b:baseRecBob(alice,a,b,check))

# examen.append(exo_with_input("bob",Tuple([alice,zero,cent]),myint,alice.check_value,coeff=2,constraints=preRecBob(alice.check_value)))



############# CORRECTION ####################
dir = (lambda x:x[-2]+"/"+x[-1])(os.getcwd().split("/"))
print("\n"*5+"*** CORRECTION ***\nFichier :",args.student,"\nCorrection :",args.corr,"\nRépertoire :",dir,"")

subst_correction(examen,fichier=args.student,out="rendu.py")
# args.student="rendu"
# for e in examen:
#     print(e.name,e.file)
#     e.file = "rendu"

# print()

moulinette(examen,test_file="rendu",output=args.feedback,correction=args.corr)




