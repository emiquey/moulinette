from abc import ABC, abstractmethod
from dataTypes import *
import copy
import sys
import func_timeout
from constraints import *

debug = False

def myprint(s):
    if debug:
        print(s)

def str_args(args):
    if len(args)==0:
        return ""
    else:
        s = str(args[0])
        for i in args[1:]:
            s +=","+str(i)
        return s

class Exercise():
    def __init__(self, name, input, output, test_file="squelette",
                 nb_test=20,erreur=2,time_limit=10,coeff=1,
                 constraints=None,correction="correction"):
        self.name = name
        self.input = input
        self.output = output
        self.corr = getattr(__import__(correction),self.name)
        self.file = test_file
        self.nb_test= min(nb_test,input.max_tests())
        self.erreur=erreur
        self.time_limit = time_limit
        self.function = None
        self.coeff=coeff
        self.result = {}
        self.type = {}
        self.exists = {}
        self.exists["value"] = False
        self.type["value"] = 0
        self.result["value"] = 0
        self.exists["done"] = False
        self.result["done"] = False
        self.exists["infos"] = ""
        self.type  ["infos"] = "" 
        self.result["infos"] = ""
        self.type  ["nb_infos"] = 0 
        self.result["nb_infos"] = 0
        if constraints!=None :
            self.constraints = constraints(self)
        else:
            self.constraints = NoConstraint()
        
    def get_function(self):
        return self.function
    
    def get_type_value(self): 
         return self.type["value"]

    def get_type_infos(self):   
        return self.type["infos"]
    
    def get_exists_value(self):
        return self.exists["value"]

    def get_exists_infos(self):   
        return self.exists["infos"]
    
    def get_result_value(self):
        return self.result["value"]

    def get_result_infos(self):   
        return self.result["infos"]

    def auto_ref(self):
        return "La fonction "+self.name

    def test_existence(self):
        try:
            self.function = getattr(__import__(self.file), self.name)
            self.exists["value"] = True
        except (ImportError,AttributeError):
            self.exists["infos"] = self.auto_ref()+" n'existe pas."          
        except SyntaxError:
            self.exists["infos"] = self.auto_ref()+" a un problème de syntaxe."

    def test_type(self,res,res_cor,my_input):
        myprint("******* Test type ************")
        myprint("res = "+str(res)+"\t| cor = "+str(res_cor))
        if self.output.check_type(res):
            self.type["value"] += 1
        elif self.type["nb_infos"] < 2 :
            self.type["nb_infos"] += 1
            self.type["infos"] += "Erreur fonction "+self.name+"\n"
            self.type["infos"] += "Entrée : " + str(my_input) + "\n"
            self.type["infos"] += "etudiant : "+str(type(res))+"\n"
            self.type["infos"] += "corrige  : "+str(type(res_cor))+"\n\n"
        
        

    def test_value(self,res,res_cor,my_input):
        myprint("******* Test value ************")
        myprint("res = "+str(res)+"\t| cor = "+str(res_cor))
        if self.output.check_value(res,res_cor):
            self.result["value"] += 1
        elif self.result["nb_infos"] < 2 :
            self.result["nb_infos"] +=1
            self.result["infos"] += "Erreur fonction "+self.name+"\n"
            self.result["infos"] += "Entrée : " + str(my_input) + "\n"
            self.result["infos"] += "etudiant : "+str(res)+"\n"
            self.result["infos"] += "corrige  : "+str(res_cor)+"\n\n"
        myprint("Test value ok")    

    def test_constraints(self,res,res_cor,my_input):
        self.constraints.test(res,res_cor,my_input)    


    def test(self,test_file="squelette"):
        self.file = test_file
        myprint("******* Test :"+self.name+" *************")
        type_infos = 0
        res_infos = 0
        if not self.exists["done"]:
            self.test_existence()
        if self.exists["value"] :
            tests = self.input.generate_list(self.nb_test)
            for my_input in tests:
                my_second_input = copy.deepcopy(my_input)
                myprint("input :"+str([*my_input]))

                try:

                    res = func_timeout.func_timeout(self.time_limit,self.function, args=[*my_input])
                    myprint("test ok :"+str(res))
                    
                    res_cor = func_timeout.func_timeout(self.time_limit,self.corr, args=[*my_second_input])
                    myprint("corr ok :"+str(res_cor))

                    
                    # Test du type
                    
                    self.test_type(res,res_cor,my_input)
                                        
                    # Test de la valeur
                    self.test_value(res,res_cor,my_input)

                    # Testing additional constraints if any
                    myprint("TEST CONTRAINTES"+str(self.constraints.test))
                    self.test_constraints(res,res_cor,my_input)
                    
                
                except func_timeout.FunctionTimedOut :
                    self.type["infos"]= self.auto_ref()+" met trop de temps à l'exécution."
                    myprint("bizarre")
                    break
                except:
                    self.type["infos"]= self.auto_ref()+" a un problème à l'exécution.\nEntrée : "+str(my_input)+"\n"
                    myprint("exec")
                    break
                                # Testing additional constraints if any
                
                
        self.result["done"] = True


    def get_note(self):
        if not self.result["done"]:
            self.test()
        note = 0
        if self.type["value"]==self.nb_test:
            note+=0.5
        elif self.type["value"]>=self.nb_test-self.erreur:
            note+=0.25
        erreurs = self.nb_test-self.type["value"]    
        if erreurs==0:
            note+=1.5
        elif erreurs <= self.erreur:
            note+=max(0,min(1,1.5-0.5*erreurs))
        note = self.constraints.malus(note)
        return note*self.coeff/2
    

class ExoApprox(Exercise):
    def __init__(self, name, input, output, limit, limit_string, 
                 test_file="squelette",nb_test=20,erreur=2,time_limit=10,coeff=2,
                 constraints=None,correction="correction"):
        super().__init__(name, input, output, test_file=test_file,nb_test=20,erreur=2,
                         time_limit=10,coeff=2,constraints=constraints,correction=correction)
        self.limit = limit
        self.limit_string = limit_string  

    def test_value(self,res,res_cor,my_input):
        if self.output.check_value(self.limit,res_cor,my_input[0]):
            self.result["value"] += 1
        elif res_infos < 2 :
            res_infos += 1
            self.result["infos"] += "Erreur fonction "+self.name+"\n"               
            self.result["infos"] += "Entrée\t: " + str_args(my_input) + "\n"              
            self.result["infos"] += self.limit_string+"\t: "+str(self.limit)+"\n"                    
            self.result["infos"] += "etudiant.e\t: "+str(res)+"\n"              
            self.result["infos"] += "|"+self.limit_string+"-res|\t: "+str(abs(self.limit-res))+"\n\n"     
                

class ExoCheckWithInput(Exercise):
    def __init__(self, name, input, output, howtocheck, 
                 test_file="squelette",nb_test=20,erreur=2,time_limit=10,coeff=2,
                 constraints=None,correction="correction"):
        super().__init__(name, input, output, test_file=test_file,nb_test=20,erreur=2,
                         time_limit=10,coeff=2,constraints=constraints,correction=correction)
        self.check = howtocheck
        
    def test_value(self,res,res_cor,my_input):
        myprint("******* Test value ************")
        myprint(str(self.check)+"\t res:"+str(res)+"\t corrcat:"+str(res_cor)+"\t"+str(my_input))
        if self.check(res,my_input):
            myprint("test val ok")
            self.result["value"] += 1
            myprint("test val ok")
        elif self.result["nb_infos"] < 2 :
            myprint("test val pas ok")
            self.result["nb_infos"] += 1
            self.result["infos"] += "Erreur fonction "+self.name+"\n"
            self.result["infos"] += "Entrée : " + str(my_input) + "\n"
            self.result["infos"] += "etudiant : "+str(res)+"\n"
            self.result["infos"] += "corrige  : "+str(res_cor)+"\n\n"   
        else:
             myprint("test val pas ok, trop d'infos")
        

   
