from abc import ABC, abstractmethod
from exercises import *
from dataTypes import *


def reformat(name,lmax):
    return name + " "*(lmax - len(name))


def moulinette(exo,test_file="squelette",output="result.txt",correction="correction"):
    ########## TESTS DE LA MOULINETTE ###########

    for e in exo:
        e.test(test_file=test_file)

    ########### CRÉATION DU FEEDBACK  ###########

    sep = "###################################\n"
    feedback = ""

    ### Existence 

    feedback += sep+"   Test existence des fonctions\n"+sep


    allright = True
    for e in exo:
        if not e.exists["value"]:
            allright = False
            feedback += "\n"+e.exists["infos"]

    if allright:
        feedback += "\nRien à signaler.\n"

    feedback += "\n"    
    allright = True


    ### Types
    
    feedback += sep+"   Test du type des fonctions\n"+sep

    for e in exo:
        if not e.type["value"]==e.nb_test:
            allright = False
            feedback += "\n"+e.type["infos"]

    if allright:
        feedback += "\nRien à signaler.\n"

    feedback += "\n"    
    allright = True    

    ### Fonctionnalité

    feedback += sep + "   Test fonctionnalité des fonctions\n" + sep

    for e in exo:
        if not e.result["value"]==e.nb_test:
            allright = False
            feedback += "\n"+e.result["infos"]

        if e.constraints.hasConstraints():
            constraints_info = e.constraints.get_infos()
            feedback += "\n"+constraints_info
            allright = allright and constraints_info == ""


    if allright:
        feedback += "\nRien à signaler.\n"

    feedback += "\n"    
    allright = True    


    feedback+= sep + sep + "\n"
    feedback+= "           Fin des tests           \n"
    feedback+= "\n" + 2*sep+"\n"

    ### Synthèse 

    feedback+= sep
    feedback+= "      Analyse des résultats        \n"
    feedback+= sep + "\n"


    ### Calcul de la note finale

    note_finale = 0
    total = 0
    for e in exo:
        note_finale += e.get_note()
        total += e.coeff

    if floor(note_finale)==note_finale:
        note_finale = int(note_finale)

    lmax = 0
    for e in exo:
        lmax = max(len(e.name),lmax)


    for e in exo:
        feedback += reformat(e.name,lmax)
        def_out = "OK" if e.exists["value"] else "KO"
        feedback += " | defined : "+def_out
        type_out = "OK" if e.type["value"]==e.nb_test else "KO"
        feedback += " | type : " + type_out
        res_out = "OK" if e.result["value"]==e.nb_test  else "KO"
        feedback += " | behave : " + res_out
        if e.constraints.okko["value"]:
            feedback += e.constraints.get_okko()
        feedback += "\n"


    feedback += "\nNote : "+str(note_finale)+" / "+str(total)+"\n\n"
    feedback += sep +"Fin de traitement\n"+sep


    ############### ÉCRITURE DU FEEDBACK ################

    fichier = open(output,"a")
    fichier.write(feedback)
    fichier.close()

    print("Évaluation terminée")