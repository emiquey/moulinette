from math import *
from random import *
import sys
from copy import *
sys.setrecursionlimit(10**6)

"""
Ce fichier contient une correction du TP noté 2, ainsi
que des tests pour chacune des fonctions : prenez le temps
de les exécuter de regarder les résultats affichés !
"""


def print_exo(n):
    print("**********************")
    print("***   Exercice",n,"   **")
    print("**********************\n")

##################
##  Exercice 1   #
##################




def somme_liste(L):
    if L==[] :
        return 0
    return L[0]+somme_liste(L[1:])


# def somme_liste(L):
#     res = 0
#     for i in L:
#         res +=i
#     return res


# print_exo(1)
# L = [randint(0,20) for i in range(10)]
# print("L =",L,"\n")
# print("somme_liste(L) =",somme_liste(L),"\n")



def somme_carres(L):
    res = 0
    for i in L:
        res += i**2
    return res

# print("somme_carres(L) =",somme_carres(L),"\n")



def ecart_type(L):
    s = somme_liste(L)
    c = somme_carres(L)
    n = len(L)
    return sqrt(1/n*c-(1/n*s)**2)

# print("ecart_type(L) = ",ecart_type(L),"\n")


##################
##  Exercice 2   #
##################


def terme(n):
    if n==0:
        return 1
    else:
        return (-1)**n/(2*n+1)+terme(n-1)
    
def terme2(n):
        s = 1
        for i in range(1,n+1):
            s += (-1)**i/(2*i+1)
        return s


# print_exo(2)
# for i in range(1,100,10):
#     print("terme(",i,") =",terme(i))

# print("\nπ/4 = ",pi/4,"\n")

def approx(eps):
    n = int((4/eps-1)/2)+1
    m = pi
    return 4*terme2(n)

# for i in range(1,6):
#     print("approx(10**-"+str(i)+") =",approx(10**-i),"\t|π - approx(10**-"+str(i)+")| =",abs(pi-approx(10**-i)))

# print()

##################
##  Exercice 3   #
##################

# print_exo(3)

def max_liste(L):
    if len(L)==1:
        return L[0]
    else:
        return max(L[0],max_liste(L[1:]))


def second_max(L):
    M = max_liste(L)
    i = 0
    m = L[0]
    # On doit s'assurer que l'initialisation n'est
    # pas faite pour une valeur égale au max
    while i<len(L) and m==M:
        i += 1
        m = L[i]
    # On peut maintenant lancer notre recherche
    for j in range(i,len(L)):
        if m<L[j]<M:
            m = L[j]
    return m

"""
Autre variante : on initialise m avec le min

def min_liste(L):
    if len(L)==1:
        return L[0]
    else:
        return min(L[0],max_liste(L[1:]))

def second_max(L):
    M = max_liste(L)
    m = min_liste(L)
    for i in L:
        if m<i<M:
            m = i
    return m
"""

# for i in range(5):
#     L = [randint(1,10) for i in range(7)]
#     print("L =",L,"\t| max = ",max_liste(L),"\t| second max =",second_max(L))
    

##################
##  Exercice 4   #
##################

# print_exo(4)
# L = [ [6.2,5.3,120,13.1], [6.3,5.5,130,13.5], [6.25,5.41,135,13.7]]

def distance_2points(x,y):
    return sqrt((x[0]-y[0])**2+(x[1]-y[1])**2+(x[2]-y[2])**2)

def dates(l):
    d = []
    for i in l:
        d.append(i[-1])
    return d

def distances(l):
    d = [0]
    for i in range(1,len(l)):
        d.append(d[i-1]+distance_2points(l[i][:-1],l[i-1][:-1]))
    return d


def vitesse(l):
    v = [0]
    d = distances(l)
    t = dates(l)
    for i in range(1,len(l)):
        v.append((d[i]-d[i-1])/(t[i]-t[i-1]))
    return v


## Fonction de génération de traces

def random_trace(n):
    l = [[randint(0,10),randint(0,10),randint(0,10),randint(0,10)]]
    for i in range(n):
        l.append(copy(l[-1]))
        for j in range(3):
            r = random()
            l[-1][j] = round(r+l[-1][j],2)
        l[-1][-1] = round(l[-1][-1]+randint(1,5)/10,1)
    return l 


# for i in range(3):
#     l = random_trace(5)
#     print("trace     =",l)
#     print("dates     =",dates(l))
#     print("distances =",distances(l))
#     print("vitesses  =",vitesses(l))
#     print()



##################
##  Exercice 5   #
##################

# print_exo(5)

def repete(chaine):
    if chaine=="":
        return ""
    else:
        return chaine[0]*2+repete(chaine[1:])

# mot = "bonjour"
# print("Repete :",mot,"->",repete(mot),"\n")

def max_bloc(chaine):
    m=0
    current=0
    for c in chaine:
        if c=='a':
            current +=1
            if current>m:
                m=current
        else:
            current = 0
    return m

def random_mot(n,char):
    c = ""
    for i in range(n):
        c += chr(randint(0,ord(char)-ord('a'))+ord("a"))
    return c

# for i in range(5):
#     mot = random_mot(randint(1,8),'c')
#     print("mot =",mot,"\t| max_bloc =",max_bloc(mot))

# print()


##################
##  Exercice 6   #
##################
# print_exo(6)


def facteur(m,M):
    diff = len(M)-len(m)
    for i in range(len(M)-len(m)):
        if M[i:i+len(m)]==m:
            return True
    return False

# def facteur(m,M):
#     diff = len(M)-len(m)
#     if diff<0 or len(M)==0:
#         return False
#     else:
#         return M[0:len(m)]==m or facteur(m,M[1:])
        
# for i in range(5):
#     m = random_mot(randint(1,3),'d')
#     M = random_mot(randint(1,20),'d')        
#     print("facteur("+m+","+M+") =",facteur(m,M))
# print()

def sous_chaine(m,M):
    i = 0
    j = 0
    while i<len(m) and j<len(M):
        if m[i]==M[j]:
            i+=1
        j+=1
    return i==len(m)


# for i in range(5):
#     m = random_mot(randint(1,5),'d')
#     M = random_mot(randint(1,15),'d')        
#     print("sous_chaine("+m+","+M+") =",sous_chaine(m,M))


# print()

##################
##  Exercice 7   #
##################

# print_exo(7)

def bob(f,a,b):
    if a==b:
        return a
    else:
        m = (a+b)//2
        r = f(m)
        if r==-1: return bob(f,m+1,b)
        if r==1: return bob(f,a,m-1)
        return m
    


# n = 50    
# def alice(x):
#     global n
#     if x>n:return 1
#     if x<n:return -1
#     return 0


# for i in range(5):
#     a = randint(0,100)
#     b = randint(a,100)
#     n = randint(a,b)
#     print("a =",a,"\tb =",b,"\tn =",n,"\t| bob :",bob(alice,a,b))

