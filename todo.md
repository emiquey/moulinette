## TODO 

[*] Rajouter les éventuels types manquants
[*] Débugger ExoCheckWithInput // Alice
[*] Type qui génère input + fonction de check / valeur output ?
[*] Possibilité d'un type dont un argument dépend des autres (ex: Σ(a,b:int).Alice(a,b) pour une recherche aux bornes non-fixes)
