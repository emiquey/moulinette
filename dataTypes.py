from abc import ABC, abstractmethod
from random import *
from math import *
import copy



### All types should inherit from this class

class Types(ABC):
    @abstractmethod
    def generate(self):
        """ /!\ To get a uniform behavior with function requiring 
        tuples of arguments, inputs/values always have to be given 
        as a (possibly unary) list. /!\ """
        pass

    def max_tests(self):
        return inf

    def generate_list(self,n,mandatory=[]):
        """ Parameters : 
         - length of the list,
         - mandatory values to include in the list
        Remember that inputs/values should always be given as 
        a (possibly unary) list."""
        l = mandatory
        for i in range(n-len(l)):
            l.append(self.generate())
        return l

    def check_type(self,x):
        return isinstance(x,self.type)

    def check_value(self,x,y):
        return x == y
    
class CheckWithInput(Types,ABC):
    @abstractmethod
    def check_value(self,x,input):
        pass

### Constants

class Constant(Types):
    def __init__(self,val):
        self.val = val
    
    def generate(self):
        return [self.val]
    
    def check_type(self,x):
        return isinstance(x,type(self.val))
    
    def generate_list(self,n,mandatory=[]):
        return [[self.val]]*n

### Natural numbers

class Int(Types):
    def __init__(self,min=0,max=100):
        """ Parameters : 
         - min/max possible values"""
        self.min = min
        self.max = max
        self.type = int
    
    def generate(self):
        return [randint(self.min,self.max)]
    
    def generate_list(self,n):
        return super().generate_list(n,[[self.min],[self.max]])
 
    

### Float

class Float(Types):
    def __init__(self,min=0,max=1,epsilon=10**-7):
        """ Parameters : 
         - min/max values
         - precision ε for equality check """
        self.min = min
        self.max = max
        self.type = float
        self.epsilon = epsilon
    
    def generate(self):
        return [(self.max-self.min)*random()+self.min]
    
    
    def check_type(self, x):
        return isinstance(x, float) or isinstance(x,int)
    
    def check_value(self, x, y,precision = None):
        precision = precision or self.epsilon
        return abs(x-y) < precision


### String

class String(Types):
    def __init__(self,cmin='a',cmax='e',lmin=10,lmax=20):
        """ Parameters : 
         - first/last possible character
         - min/max length """
        self.cmin = cmin
        self.cmax = cmax
        self.lmin = lmin
        self.lmax = lmax
        self.type = str
    
    def generate(self):
        n = randint(self.lmin,self.lmax)
        s = ""
        for i in range(n):
            s += chr(randrange(ord(self.cmin),ord(self.cmax))) 
        return [s]
    
    def generate_list(self, n):
        return super().generate_list(n, [[""]])
    
### Empty type

class Empty(Types):
    def __init__(self):
        self.type = type(None)
    
    def generate(self):
        return [[]]
    
    def generate_list(self,n):
        return [[]]
    
    def max_tests(self):
        return 1



### Booleans

class Bool(Types):
    def __init__(self):
        self.type = bool

    def generate(self):
        if (randrange(2)):
            return True
        else:
            return False
  

### Lists of natural numbers

# class IntList(Types):
#     def __init__(self,vmin=0,vmax=100,lmin=0,lmax=20,nmin=0):
#         self.int = Int(vmin,vmax)
#         self.lmin = max(lmin,nmin)
#         self.lmax = max(nmin,lmax)
#         self.nmin = nmin #distincts values guaranteed
#         self.type = list
    
#     def generate(self):
#         n=randrange(self.lmin,self.lmax)
#         l=[[]]
#         if self.nmin>0:
#             # Ensuring it satisfies the "nmin distinct values" condition
#             l=[[self.int.generate()[0]]]
#             for i in range(1,self.nmin):
#                 x = self.int.generate()[0]
#                 while x in l[0]:
#                     x = self.int.generate()[0]
#                 l[0].append(x)
#         l[0]+=[self.int.generate()[0] for i in range(self.nmin,n)]
        
    
#     def generate_list(self,n):
#         fst = randrange(self.lmin,self.lmax)
#         mandatory = []
#         if self.lmin==0:
#             mandatory.append([[]])
#         mandatory.append([[x[0] for x in self.int.generate_list(fst)]])
#         # Ensuring it satisfies the "nmin distinct values" condition
#         for i in range(1,self.nmin):
#                 while mandatory[1][i] in mandatory[1][:i]:
#                     mandatory[1][i] = self.int.generate()[0]
#         return super().generate_list(n,mandatory)
        


### Lists of any (?) Types


class List(Types):
    def __init__(self,vmin=0,vmax=100,lmin=0,lmax=20,nmin=0,T:Types=None):
        self.lmin = max(lmin,nmin)
        self.lmax = max(nmin,lmax)
        self.nmin = nmin #distincts values guaranteed
        self.type = list
        if T==None:
            T = Int
        self.content = T(vmin,vmax)
    
    def generate(self):
        n=randint(self.lmin,self.lmax)
        l=[[]]
        if self.nmin>0:
            # Ensuring it satisfies the "nmin distinct values" condition
            l=[[self.content.generate()[0]]]
            for i in range(1,self.nmin):
                x = self.content.generate()[0]
                while x in l[0]:
                    x = self.content.generate()[0]
                l[0].append(x)
        l[0]+=[self.content.generate()[0] for i in range(self.nmin,n)]
        return l
        
    
    def generate_list(self,n):
        fst = randint(self.lmin,self.lmax)
        mandatory = []
        if self.lmin==0:
            mandatory.append([[]])
        mandatory.append([[x[0] for x in self.content.generate_list(fst)]])
        # Ensuring it satisfies the "nmin distinct values" condition
        for j in range(len(mandatory)):
            for i in range(1,self.nmin):
                while mandatory[j][0][i] in mandatory[j][0][:i]:
                    mandatory[j][0][i] = self.content.generate()[0]
        return super().generate_list(n,mandatory)


### For functions taking several inputs

class Tuple(Types):
    def __init__(self,l):
        self.types = l
        self.len = len(l)
    
    def generate(self):
        return [x.generate()[0] for x in self.types]
    
    def generate_list(self,n,mandatory=[]):
        m = n - len(mandatory)
        l = [x.generate_list(m) for x in self.types]
        res = mandatory
        res = []
        for i in range(m):
            aux = []
            for j in range(self.len):
                aux.append(l[j][i][0])
            res.append(aux)
        return res
 
    def check_type(self,x,y):
        ok = len(x)==self.len and len(y)==self.len
        if ok:
            for i in range(self.len):
                ok = ok and self.types[i].check_type(x[i],y[i])
        return ok
    
    def check_value(self,x):
        ok = len(x)==self.len
        if ok:
            for i in range(self.len):
                ok = ok and self.types[i].check_value(x[i])
        return ok
    
    def max_tests(self):
        l = inf
        for t in self.types:
            l = min(l,t.max_tests())
        return l

    

### Alice & Bob playing over a mysterious number

class Mysterious():
    def __init__(self,a=0,b=100,maxplay=10,name=0):
        self.mystere=randint(a,b)
        self.count = maxplay
        self.name = str(name)

    def f(self,x):
        #print("f",self.name,"x=",x," reste ",self.count)
        if x==-42:
            return self.mystere
        if self.count<=0:
            return 0
        self.count -= 1
        if x<self.mystere: return -1
        if x>self.mystere: return 1
        return 0

class Alice(CheckWithInput):
    def __init__(self):
        self.list =[]
        self.tocheck = -1
        self.secret = {}

    def max_tests(self):
        return 2

    def generate(self):
        m=Mysterious()
        return [lambda x:m.f(x)]
    
    def generate_list(self,n):
        for i in range(n):
            m = Mysterious(name=len(self.list))
            self.list.append(m)
            self.secret[m.f]=m.mystere
        return [[x.f] for x in self.list]
    
    def check_value(self, x, input):
        """ TODO : à débugger"""
       # print("Check :",input,self.secret[input[0]],x)
        # for i in self.list:
        #     if i.f == input[0]:
        #         print("Trouvé :",i.mystere)
        #print("backdoor ",input[0](-42))
        #print("result : ",x == self.secret[input[0]])
        return x == self.secret[input[0]]
    


### Trace GPX


class GPXTrace(Types):
    def generate(self):
        n = randint(2,20)
        l = [[randint(0,10),randint(0,10),randint(0,10),randint(0,10)]]
        for i in range(n):
            l.append(copy.copy(l[-1]))
            for j in range(3):
                r = random()
                l[-1][j] = round(r+l[-1][j],2)
            l[-1][-1] = round(l[-1][-1]+randint(1,5)/10,1)
        return [l]
    
    def check_type(self,x):
        raise Exception("Trace shouldn't be a result")
    
    def check_value(self,x,y):
        raise Exception("Trace shouldn't be a result")
    
    def generate_list(self,n,mandatory=[]):
        l = mandatory
        for i in range(n-len(l)):
            l.append(self.generate())
        return l
    
        
    