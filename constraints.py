
from bdb import Bdb
from exercises import *
from dataTypes import *
import inspect

import sys


def str_args(args):
    if len(args)==0:
        return ""
    else:
        s = str(args[0])
        for i in args[1:]:
            s +=","+str(i)
        return s

class RecursionDetected(Exception):
    pass

class RecursionDetector(Bdb):
    def do_clear(self, arg):
        pass

    def __init__(self, *args):
        Bdb.__init__(self, *args)
        self.stack = set()

    def user_call(self, frame, argument_list):
        code = frame.f_code
        if code in self.stack:
            raise RecursionDetected
        self.stack.add(code)

    def user_return(self, frame, return_value):
        self.stack.remove(frame.f_code)

def test_recursion(func):
    detector = RecursionDetector()
    detector.set_trace()
    try:
        func()
    except RecursionDetected:
        return True
    else:
        return False
    finally:
        sys.settrace(None)




# Contraintes en tout genre

class Constraint(ABC):
    constraint = True
    okko ={}
    okko["value"]=False
    infos = ""

    def hasConstraints(self):
        return self.constraint
    
    @abstractmethod
    def test(self,res,res_cor_input):
        pass
    
    def malus(self,note):
        return note

    def get_infos(self):
        return self.infos

    def get_okko(self):
        return ""


class NoConstraint(Constraint):
    def __init__(self):
        self.constraint = False
    
    def test(self,res,res_cor,input):
        return None
    
    
 
class IsRecursive(Constraint):
        
    def __init__(self,exo,base):
        self.okko["value"]=True
        self.okko["infos"]=""
        self.constraint = True
        self.basecase = base
        self.fault = 0
        self.exo = exo
        self.infos =''
        
           
    def isBaseCase(self,*i):
        return self.basecase(*i)
    
    def test(self,res,res_cor,my_input):
        #print("is_rec :",res,res_cor,my_input,self.isBaseCase(*my_input))
        if self.fault <= 3 and not(self.isBaseCase(*my_input)):
            if not(test_recursion(lambda:self.exo.get_function()(*my_input))):
                
                if self.fault <= 2:
                    self.infos += "Erreur fonction "+self.exo.name+"("+str_args(my_input)+") : "
                    self.infos += "devrait être récursive.\n"                   
                else:
                    self.infos += "Erreur fonction "+self.exo.name+" :  etc.  (problème de récursivité récurrent)\n"
                self.fault +=1

        
    def get_okko(self):
        res_out = "OK" if  self.exo.exists["value"] and  self.exo.type["value"]==self.exo.nb_test and self.exo.result["value"]==self.exo.nb_test and self.fault <= 1 else "KO"
        return  " | recursive : " + res_out

    def malus(self,note):
        if self.fault<= 1:
            return note
        else :
            return max(note-1,0.5) if note>0 else 0
    
   


class DontUse(Constraint):
    
    def __init__(self,exo,banned):
        self.exo = exo
        self.constraint=True
        infos =""
        self.done = False
        self.banned = banned
                        
    def test(self,res,res_cor,my_input):
        if not self.done :
            for j in self.banned :
                if j in self.exo.get_function().__code__.co_names:
                    self.infos += "Erreur fonction "+self.exo.name+" : "
                    self.infos += "utilisation de "+j+" interdite.\n"
            self.done = True
        

        if self.infos != "":
            self.exo.result["value"] = 0
    
    def malus(self,note):
        if self.infos == "":
            return note
        else :
            return min(note,0.5) 
        
class DontUseTest(Constraint):
    
    def __init__(self,exo,func,msg):
        self.exo = exo
        self.constraint=True
        infos =""
        self.done = False
        self.check = func
        self.msg= msg
                        
    def test(self,res,res_cor,my_input):
        if not self.done :
            for j in inspect.getsourcelines(self.exo.get_function().__code__)[0]:
                    if not(self.check(j)):
                        self.infos += "Erreur fonction "+self.exo.name+" à la ligne : \n"+j
                        self.infos += self.msg+"\n\n"
            self.done = True
            
        

        if self.infos != "":
            self.exo.result["value"] = 0
        
    
    def malus(self,note):
        if self.infos == "":
            return note
        else :
            return min(note,0.5) 

class RecDontUseTest(IsRecursive):
    
    def __init__(self,exo,base,func,msg):
        super().__init__(exo,base)
        self.done = False
        self.infos = ""
        self.check = func
        self.msg= msg
        self.ok = True
                        
    def test(self,res,res_cor,my_input):
        super().test(res,res_cor,my_input)

        if not self.done :
            for j in inspect.getsourcelines(self.exo.get_function().__code__)[0]:
                    if not(self.check(j)):
                        self.infos += "Erreur fonction "+self.exo.name+" à la ligne : \n"+j
                        self.infos += self.msg+"\n\n"
                        self.ok = False
            self.done = True
            
        

        if not self.ok:
            self.exo.result["value"] = 0
        
    
    def malus(self,note):
        if self.ok and self.fault<= 1:
            return note
        elif self.ok :
            return max(note-1,0.5) if note>0 else 0
        else:
            return min(note,0.5) 



# class Equality(Constraint):
#     a,b = 0,0
#     def __init__(self,x,y):
#         self.constraint = True
#         self.a = x
#         self.b = y
        
#     def getConstraint(self):
#         return self.a,self.b
        

# class Constant(Constraint):
#     val = 0
    
#     def __init__(self,x):
#         self.constraint = True
#         self.val = x
        
#     def getConstraint(self):
#         return self.val


# class WithValues(Constraint):
#     val = []
    
#     def __init__(self,l):
#         self.constraint = True
#         for i in l:
#             self.val.append(i)
        
#     def getValues(self):
#         return self.val
    
# class Bounded(Constraint):
#     low,high = 0,0
    
#     def __init__(self,x,y):
#         self.constraint = True
#         self.low = x
#         self.high = y
        
#     def getBounds(self):
#         return self.low,self.high

# class BoundedWith(Bounded,WithValues):
#      def __init__(self,x,y,l):
#         self.constraint = True
#         self.low = x
#         self.high = y
#         for i in l:
#             self.val.append(i)

    

# class Shuffled(Constraint):
#     def __init__(self):
#         self.constraint = True
        
#     def getConstraint(self):
#         return None

